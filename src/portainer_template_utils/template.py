"""A module for producing a JSON file of Portainer-compatible service templates from a folder of compose files."""

from enum import IntEnum
from pathlib import Path

from pydantic import BaseModel

from .compose import ServiceEnvVar, ServiceConfig, read_library, get_compose_paths


class TemplateType(IntEnum):
    """The types of Portainer templates and their integer value for JSON files."""

    CONTAINER = 1
    SWARM_STACK = 2
    COMPOSE_STACK = 3


class TemplateRepository(BaseModel):
    """Container for data that defines a Portainer template's location."""

    stackfile: str
    url: str


class PortainerTemplate(BaseModel):
    """Model for Portainer template JSON definitions.

    https://docs.portainer.io/advanced/app-templates/format
    """

    type: TemplateType
    title: str
    description: str
    categories: list[str]
    platform: str
    logo: str
    repository: TemplateRepository
    env: list[ServiceEnvVar]


class PortainerTemplateContainer(BaseModel):
    """Model for container of multiple Portainer template JSON definitions."""

    version: str = "2"
    templates: list[PortainerTemplate]


def generate_template_obj(library_data: list[ServiceConfig]) -> PortainerTemplateContainer:
    """Generate a JSON templates file from a list of ServiceConfig objects."""

    return PortainerTemplateContainer(
        templates=[
            PortainerTemplate(
                type=TemplateType(service.type),
                title=service.title,
                description=service.description,
                categories=service.categories,
                platform=service.platform,
                logo=service.logo_url,
                repository=TemplateRepository(stackfile=service.filename, url=service.repo_url),
                env=service.env_vars,
            )
            for service in library_data
        ]
    )


def generate_from_lib(library_path: Path) -> PortainerTemplateContainer:
    """Generate a JSON templates file using a path to a library of compose files."""

    library_data = [dataset for dataset in read_library(get_compose_paths(library_path=library_path))]

    return generate_template_obj(library_data=library_data)
