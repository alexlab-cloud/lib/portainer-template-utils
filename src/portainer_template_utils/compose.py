"""Module for handling directories (libraries) containing multiple services defined in compose files."""

import glob
import re
from pathlib import Path
from typing import Self

from loguru import logger

from pydantic import BaseModel

DEFAULT_VAR_PREFIX = "#& "
DEFAULT_ENV_VAR_PREFIX = "#% "


class ServiceEnvVar(BaseModel):
    """Data container for environment variables, which require names, labels, descriptions, and default values.

    Portainer templates allow environment variables to be supplied with these parameters for more intuitive
    use in the app UI.
    """

    name: str
    label: str = ""
    description: str = ""
    default: str = ""


class ServiceConfig(BaseModel):
    """Simple metadata type for services defined in compose files."""

    filename: str
    type: int
    title: str
    description: str
    categories: list[str]
    platform: str
    logo_url: str
    repo_url: str
    env_vars: list[ServiceEnvVar]

    @classmethod
    def from_compose_file(
        cls,
        compose_file_path: Path,
        var_prefix: str = DEFAULT_VAR_PREFIX,
        env_var_prefix: str = DEFAULT_ENV_VAR_PREFIX,
    ) -> Self:
        """Build a ServiceConfig object from a compose YAML file.

        The compose file must contain the same field names in a comment block, with each field prepended by a specified
        prefix. Environment variables must be specified after their own prefix with a description, and follow this
        format:

            #% ENV_VAR_NAME: Label (Description) [default]

        Here's a full example of both variable types, which would be included at the top of a compose file:

            # Metadata variables (required)
            #& filename: docker-compose.uptime-kuma.yaml
            #& title: Uptime Kuma
            #& description: Uptime Kuma monitors websites for...
            #& categories: Monitoring, Notifications
            #& platform: linux
            #& logo_url: https://example.com/logo.png
            #& repo_url: https://gitlab.com/namespace/custom-app-stacks

            # Environment variables (optional; as many as needed)
            #% SERVICE_NAME: Name of the service (No spaces or periods) [uptime-kuma]
            #% DATA_LOCATION: Volume location (Example: my-drive/uptime-kuma) [/pillar/apps/uptime-kuma]

        The var_prefix and env_var_prefix module variables can be changed to use different prefixes.
        """

        with open(compose_file_path, encoding="utf-8") as compose_file:
            env_vars: list[ServiceEnvVar] = []

            for line in compose_file.readlines():
                if re.search(var_prefix, line):
                    line_data = line[len(var_prefix) : -1].split(": ", 1)
                    if line_data[0] == "title":
                        title = line_data[1]
                    elif line_data[0] == "type":
                        try:
                            template_type = int(line_data[1])
                        except ValueError as err:
                            # Couldn't cast the input value to an int
                            logger.error(err)

                    elif line_data[0] == "description":
                        description = line_data[1]
                    elif line_data[0] == "categories":
                        categories = line_data[1].split(", ")
                    elif line_data[0] == "platform":
                        platform = line_data[1]
                    elif line_data[0] == "logo_url":
                        logo_url = line_data[1]
                    elif line_data[0] == "repo_url":
                        repo_url = line_data[1]

                elif re.search(env_var_prefix, line):
                    line_data = line[len(env_var_prefix) : -1].split(": ", 1)

                    env_var_data = ServiceEnvVar(name=line_data[0].strip())

                    try:
                        line_data_details = re.split(r"\(|\[", line_data[1])
                        env_desc = line_data_details[1]
                        env_hold = line_data_details[2]

                        env_var_data.label = line_data_details[0].strip()
                        env_var_data.description = env_desc[:-2].strip()
                        env_var_data.default = env_hold[:-1].strip()
                    except IndexError:
                        try:
                            line_data_details = re.split(r"\(|\[", line_data[1])
                            env_desc = line_data_details[1]

                            env_var_data.label = line_data_details[0].strip()
                            env_var_data.description = env_desc[:-1].strip()
                        except IndexError:
                            env_var_data.label = line_data[1].strip()

                    env_vars.append(env_var_data)

        return cls(
            filename=compose_file_path.as_posix(),
            type=template_type,
            title=title,
            description=description,
            categories=categories,
            platform=platform,
            logo_url=logo_url,
            repo_url=repo_url,
            env_vars=env_vars,
        )


def get_compose_paths(library_path: Path) -> list[Path]:
    """Return a clean list of compose file paths."""

    return [Path(item) for item in glob.glob(pathname=f"{library_path}/**/*compose*.y*ml")]


def read_library(compose_paths: list[Path]) -> list[ServiceConfig]:
    """Build service configuration data from a collection of compose files."""

    return [ServiceConfig.from_compose_file(compose_path) for compose_path in compose_paths]


def ingest_compose_library(library_path: Path) -> list[ServiceConfig]:
    """Build service configuration data from a compose library path."""

    return read_library(compose_paths=get_compose_paths(library_path=library_path))
