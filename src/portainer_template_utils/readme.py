"""A module for generating README.md files from compose files and Jinja templates."""

from pathlib import Path

from jinja2 import Environment, FileSystemLoader
from pydantic import BaseModel

from .compose import get_compose_paths, read_library


class ReadmeCustomization(BaseModel):
    """Data container for customizable text in the generated README for the Portainer templates."""

    title: str = "portainer-templates"
    subtitle: str = "Easily deployable Portainer service templates."
    body_text: str = ""
    footer_text: str = ""


def generate_from_lib(library_path: Path, readme_custom_text: ReadmeCustomization = ReadmeCustomization()) -> str:
    """Generate a README.md file from a folder of compose files."""

    # `autoescape=False` → https://docs.openstack.org/bandit/1.4.0/plugins/jinja2_autoescape_false.html
    env = Environment(loader=FileSystemLoader("src/portainer_template_utils/templates"), autoescape=True)

    readme_template = env.get_template(name="README.md.j2")

    compose_data = [dataset.model_dump() for dataset in read_library(get_compose_paths(library_path=library_path))]

    return readme_template.render(services=compose_data, custom_text=readme_custom_text.model_dump())


def write_md_file(file_contents: str) -> None:
    """Write a README.md file to disk."""

    with open(file="README_test.md", mode="wt", encoding="utf-8") as readme_file:
        readme_file.write(file_contents)
