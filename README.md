# portainer-template-utils

<div float="left">
    <a href="https://gitlab.com/alexlab-cloud/lib/python/portainer-template-utils/-/commits/main"><img alt="pipeline status" src="https://gitlab.com/alexlab-cloud/lib/python/portainer-template-utils/badges/main/pipeline.svg?style=flat-square" />
    <a href="https://gitlab.com/alexlab-cloud/lib/python/portainer-template-utils/-/commits/main"><img alt="coverage report" src="https://gitlab.com/alexlab-cloud/lib/python/portainer-template-utils/badges/main/coverage.svg?style=flat-square" />
    <a href="https://gitlab.com/alexlab-cloud/lib/python/portainer-template-utils/-/releases"><img alt="Latest Release" src="https://gitlab.com/alexlab-cloud/lib/python/portainer-template-utils/-/badges/release.svg?style=flat-square" />
</div>

---

A Python library to assist in making and documenting Portainer templates based on your existing compose files.
Implementations for compose file data and Portainer template data are separate to allow extension of file metadata beyond what the Portainer
template schema allows.

## Features

Given a collection of compose files:

- Portainer template file (`portainer-templates.json`) generation
- README generation from data

## Installation

`pip install portainer-template-utils --index-url https://gitlab.com/api/v4/projects/51476638/packages/pypi/simple`

### Browsing Package Versions

You can also view deployed packages in this project's [GitLab Package Registry](https://gitlab.com/alexlab-cloud/lib/python/portainer-template-utils/-/packages).

## Usage

Organize your compose files into a directory. Then, use `portainer-template-utils` to generate a `portainer-templates.json`
and `README.md` for your collection.

### Portainer Templates JSON

    import portainer_template_utils as ptu

    library_data = compose.read_library(compose.get_compose_paths(library_path="/path/to/compose/files"))

    template_obj = ptu.template.generate_obj(library_data=library_data)

### README Generation

    import portainer_template_utils as ptu

    library_data = compose.read_library(compose.get_compose_paths(library_path="/path/to/compose/files"))

    readme_str = ptu.readme.generate_from_lib(library_path=library_data)

---

<sub>Emoji used for repository logo designed by <a href="https://openmoji.org/">OpenMoji</a> – the open-source emoji and icon project. License: CC BY-SA 4.0</sub>
