"""Common resources shared between test modules."""

from pathlib import Path

import pytest


@pytest.fixture
def compose_library_path() -> Path:
    """Return the compose library path used for testing."""

    return Path("tests/assets/compose-library")
