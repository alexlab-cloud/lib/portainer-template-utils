"""Tests for portainer_template_utils.compose."""

import unittest
from pathlib import Path

from portainer_template_utils import compose


def test_get_compose_paths(compose_library_path: Path) -> None:
    """Ensure compose file paths are correctly resolved and organized into a list."""

    case = unittest.TestCase()

    compose_file_paths = compose.get_compose_paths(library_path=compose_library_path)

    case.maxDiff = None
    case.assertCountEqual(
        compose_file_paths,
        [
            Path("tests/assets/compose-library/immich/docker-compose.immich.yml"),
            Path("tests/assets/compose-library/linkding/docker-compose.linkding.yml"),
        ],
    )


def test_read_collection(compose_library_path: Path) -> None:
    """Ensure data from compose files is read into models correctly."""

    case = unittest.TestCase()

    dataset = compose.read_library(compose.get_compose_paths(library_path=compose_library_path))

    linkding_mock = compose.ServiceConfig(
        filename="tests/assets/compose-library/linkding/docker-compose.linkding.yml",
        title="linkding",
        type=3,
        description="linkding is...",
        categories=["bookmarks"],
        platform="linux",
        repo_url="https://gitlab.com/alexlab-cloud/infrastructure/homelab-service-templates",
        logo_url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQH4gm3zI3a4mAZLyCbf6n8rrvUvLJw7Pu7JH8vBaEmmg&s",
        env_vars=[
            compose.ServiceEnvVar(
                name="SERVICE",
                label="Name of the service",
                description="No spaces or points",
                default="linkding",
            ),
            compose.ServiceEnvVar(
                name="PORT",
                label="Port mapping for the host system",
                description="Valid port number",
                default="9090",
            ),
            compose.ServiceEnvVar(
                name="DATA_LOCATION",
                label="Volume root location",
                description="Example: /docker-app-data",
                default="/docker-app-data",
            ),
        ],
    )

    immich_mock = compose.ServiceConfig(
        filename="tests/assets/compose-library/immich/docker-compose.immich.yml",
        title="immich",
        type=3,
        description="immich is...",
        categories=["images", "storage"],
        platform="linux",
        repo_url="https://gitlab.com/alexlab-cloud/infrastructure/homelab-service-templates",
        logo_url="https://play-lh.googleusercontent.com/ZpV8EMn8XZ_8V1GipeeZ4y-xeXJiQD5vlJkLTSJ7NN1ty9ZdHG6AVDA-71QsjsaL4A",  # pylint: disable=line-too-long # noqa: E501
        env_vars=[
            compose.ServiceEnvVar(
                name="SERVICE",
                label="Name of the service",
                description="No spaces or points",
                default="immich",
            ),
            compose.ServiceEnvVar(
                name="PORT",
                label="Port mapping for the host system",
                description="Valid port number",
                default="9090",
            ),
            compose.ServiceEnvVar(
                name="DATA_LOCATION",
                label="Volume location",
                description="Example: /docker-app-data/service-name",
                default="/docker-app-data/immich",
            ),
        ],
    )

    mock_data = [linkding_mock, immich_mock]

    case.maxDiff = None
    case.assertCountEqual(dataset, mock_data)
