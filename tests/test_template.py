"""Tests for portainer_template_utils.template."""

import unittest
from pathlib import Path

import pytest

from portainer_template_utils import template, compose


@pytest.fixture(name="template_sample")
def fixture_template_sample() -> template.PortainerTemplateContainer:
    """Test fixture providing a sample template object."""

    return template.PortainerTemplateContainer(
        version="2",
        templates=[
            template.PortainerTemplate(
                type=template.TemplateType.COMPOSE_STACK,
                title="linkding",
                description="linkding is...",
                categories=["bookmarks"],
                platform="linux",
                logo="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQH4gm3zI3a4mAZLyCbf6n8rrvUvLJw7Pu7JH8vBaEmmg&s",  # pylint: disable=line-too-long # noqa: E501
                repository=template.TemplateRepository(
                    stackfile="tests/assets/compose-library/linkding/docker-compose.linkding.yml",
                    url="https://gitlab.com/alexlab-cloud/infrastructure/homelab-service-templates",
                ),
                env=[
                    template.ServiceEnvVar(
                        name="SERVICE",
                        label="Name of the service",
                        description="No spaces or points",
                        default="linkding",
                    ),
                    template.ServiceEnvVar(
                        name="PORT",
                        label="Port mapping for the host system",
                        description="Valid port number",
                        default="9090",
                    ),
                    template.ServiceEnvVar(
                        name="DATA_LOCATION",
                        label="Volume root location",
                        description="Example: /docker-app-data",
                        default="/docker-app-data",
                    ),
                ],
            ),
            template.PortainerTemplate(
                type=template.TemplateType.COMPOSE_STACK,
                title="immich",
                description="immich is...",
                categories=["images", "storage"],
                platform="linux",
                logo="https://play-lh.googleusercontent.com/ZpV8EMn8XZ_8V1GipeeZ4y-xeXJiQD5vlJkLTSJ7NN1ty9ZdHG6AVDA-71QsjsaL4A",  # pylint: disable=line-too-long # noqa: E501
                repository=template.TemplateRepository(
                    stackfile="tests/assets/compose-library/immich/docker-compose.immich.yml",
                    url="https://gitlab.com/alexlab-cloud/infrastructure/homelab-service-templates",
                ),
                env=[
                    template.ServiceEnvVar(
                        name="SERVICE",
                        label="Name of the service",
                        description="No spaces or points",
                        default="immich",
                    ),
                    template.ServiceEnvVar(
                        name="PORT",
                        label="Port mapping for the host system",
                        description="Valid port number",
                        default="9090",
                    ),
                    template.ServiceEnvVar(
                        name="DATA_LOCATION",
                        label="Volume location",
                        description="Example: /docker-app-data/service-name",
                        default="/docker-app-data/immich",
                    ),
                ],
            ),
        ],
    )


def test_generate_template_obj(
    compose_library_path: Path, template_sample: template.PortainerTemplateContainer
) -> None:
    """Ensure compose file paths are correctly resolved and organized into a list."""

    case = unittest.TestCase()

    library_data = compose.read_library(compose.get_compose_paths(library_path=compose_library_path))

    template_obj = template.generate_template_obj(library_data=library_data)

    case.maxDiff = None
    case.assertCountEqual(template_obj.templates, template_sample.templates)


def test_generate_from_lib(compose_library_path: Path, template_sample: template.PortainerTemplateContainer) -> None:
    """Ensure PortainerTemplateContainer generation from a library path returns the correct structure."""

    case = unittest.TestCase()

    template_obj = template.generate_from_lib(library_path=compose_library_path)

    case.maxDiff = None
    case.assertCountEqual(template_obj.templates, template_sample.templates)
