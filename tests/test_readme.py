"""Tests for portainer_template_utils.readme."""

from pathlib import Path

import pytest

from portainer_template_utils import readme


@pytest.fixture(name="readme_order_one")
def fixture_readme_order_one():
    return """# portainer-templates

Easily deployable Portainer service templates.



---

## Available Service Templates

<table>
<tr>
    <th>App</th>
    <th>Description</th>
    <th>Platform</th>
</tr>
<tbody>
    <tr>
    <td>
        <a href="https://gitlab.com/alexlab-cloud/infrastructure/homelab-service-templates">linkding</a>
    </td>
    <td>
        linkding is...
    </td>
    <td>
        linux
    </td>
    </tr>
    <tr>
    <td>
        <a href="https://gitlab.com/alexlab-cloud/infrastructure/homelab-service-templates">immich</a>
    </td>
    <td>
        immich is...
    </td>
    <td>
        linux
    </td>
    </tr></tbody>
</table>

---

    """.replace(" ", "")


@pytest.fixture(name="readme_order_two")
def fixture_readme_order_two():
    return """# portainer-templates

Easily deployable Portainer service templates.



---

## Available Service Templates

<table>
<tr>
    <th>App</th>
    <th>Description</th>
    <th>Platform</th>
</tr>
<tbody>
    <tr>
    <td>
        <a href="https://gitlab.com/alexlab-cloud/infrastructure/homelab-service-templates">immich</a>
    </td>
    <td>
        immich is...
    </td>
    <td>
        linux
    </td>
    </tr>
    <tr>
    <td>
        <a href="https://gitlab.com/alexlab-cloud/infrastructure/homelab-service-templates">linkding</a>
    </td>
    <td>
        linkding is...
    </td>
    <td>
        linux
    </td>
    </tr></tbody>
</table>

---

    """.replace(" ", "")


def test_template_readme_generation(compose_library_path: Path, readme_order_one: str, readme_order_two: str) -> None:
    """Ensure README text is properly generated."""

    generated_readme = readme.generate_from_lib(library_path=compose_library_path).replace(" ", "")

    assert generated_readme == readme_order_one or generated_readme == readme_order_two
