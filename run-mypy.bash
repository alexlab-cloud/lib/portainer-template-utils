#!/usr/bin/env bash

# https://jaredkhan.com/blog/mypy-pre-commit
# A script for running mypy with all dependencies installed.

set -o errexit

# Change directory to the project root directory.
cd "$(dirname "$0")"

# Install the dependencies into the mypy env. This can take seconds to run.
pip install --editable . \
  --retries 1 \
  --no-input \
  --quiet

# Run on all files, ignoring the paths passed to this script so as not to miss type errors.
#
# Notes:
# - Use the namespace-packages flag and specify the package to run on explicitly.
# - Do not use --ignore-missing-imports as this can give us false confidence in results.
mypy --package portainer_template_utils --namespace-packages
